//
//  StatusMenuController.swift
//  AutoHueControl
//
//  Created by Moe on 2018-06-07.
//  Copyright © 2018 Abushawish. All rights reserved.
//

import Cocoa

class StatusMenuController: NSObject {
    
    private static let MENU_ITEM_SETUP_LIGHTS = "Setup Hue Lights"
    private static let MENU_ITEM_PREFERENCES = "Preferences"
    public static let MENU_HUE_MENU_ICON = "hueMenuIcon"
    
    @IBOutlet weak var hueStatusMenu: NSMenu!
    
    let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    var setupPreferencesWindow: SetupWindow!
    var preferencesWindow: PreferencesWindow!
    
    override func awakeFromNib() {
        setMenuIcon()
        
        let bridgeAccessConfig = HueController.readBridgeAccessConfig()
        
        if (bridgeAccessConfig == nil) {
            addMenuItem(title: StatusMenuController.MENU_ITEM_SETUP_LIGHTS, action: #selector(StatusMenuController.setupBridge(_:)))
        } else {
            addMenuItem(title: StatusMenuController.MENU_ITEM_PREFERENCES, action: #selector(StatusMenuController.pref(_:)))
        }
        
        addWorkspaceNotification()
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    @objc func setupBridge(_ sender: NSMenuItem) {
        if (setupPreferencesWindow != nil && setupPreferencesWindow.isWindowLoaded) {
            setupPreferencesWindow.bringToFront()
        } else {
            setupPreferencesWindow = SetupWindow() { success in
                self.setupPreferencesWindow.close()
                self.hueStatusMenu.removeItem(at: 0)
                
                let preferencesWindow = PreferencesWindow()
                preferencesWindow.showWindow(nil)
                self.addMenuItem(title: StatusMenuController.MENU_ITEM_PREFERENCES, action: #selector(StatusMenuController.pref(_:)))
            }
            setupPreferencesWindow.showWindow(nil)
        }
    }
    
    @objc func pref(_ sender: NSMenuItem) {
        if (preferencesWindow != nil && preferencesWindow.isWindowLoaded) {
            preferencesWindow.bringToFront()
        } else {
            preferencesWindow = PreferencesWindow()
            preferencesWindow.showWindow(nil)
        }
    }
    
    private func setMenuIcon() {
        let icon = NSImage(named: NSImage.Name(rawValue: StatusMenuController.MENU_HUE_MENU_ICON))
        icon?.isTemplate = true
        statusItem.menu = hueStatusMenu
        statusItem.image = icon
    }
    
    private func addMenuItem(title: String, action selector: Selector?) {
        let menuItem = NSMenuItem(title:title, action: selector, keyEquivalent:"")
        menuItem.target = self
        hueStatusMenu.addItem(menuItem)
    }
    
    @objc func onWakeNote(note: NSNotification) {
        HueController.toggleSavedLights(onOff: true)
    }
    
    @objc func onSleepNote(note: NSNotification) {
        HueController.toggleSavedLights(onOff: false)
    }
    
    private func addWorkspaceNotification() {
        NSWorkspace.shared.notificationCenter.addObserver(
            self, selector: #selector(onWakeNote(note:)),
            name: NSWorkspace.screensDidWakeNotification, object: nil)
        
        NSWorkspace.shared.notificationCenter.addObserver(
            self, selector: #selector(onSleepNote(note:)),
            name: NSWorkspace.screensDidSleepNotification, object: nil)
    }
}
