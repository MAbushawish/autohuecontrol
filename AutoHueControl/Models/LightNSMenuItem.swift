//
//  LightNSMenuItem.swift
//  AutoHueControl
//
//  Created by Moe on 2018-07-06.
//  Copyright © 2018 Abushawish. All rights reserved.
//

import Foundation
import Cocoa
import SwiftyHue

class LightNSMenuItem: NSMenuItem {
    var associatedLight : Light

    init(title: String, action selector: Selector?, keyEquivalent charCode: String, light: Light) {
        self.associatedLight = light
        super.init(title: title, action: selector, keyEquivalent: charCode)
        self.target = self
    }
    
    required init(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
