//
//  File.swift
//  Mac Hue
//
//  Created by Moe on 2018-06-02.
//  Copyright © 2018 Abushawish. All rights reserved.
//

import Foundation

class HueBridgeConfigModel {
    var uuid: String?
    var ip: String?
    var username: String?
}
