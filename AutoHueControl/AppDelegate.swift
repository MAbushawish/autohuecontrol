//
//  AppDelegate.swift
//  AutoHueControl
//
//  Created by Moe on 2018-06-07.
//  Copyright © 2018 Abushawish. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    func applicationDidFinishLaunching(_ aNotification: Notification) {
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
    }
}

