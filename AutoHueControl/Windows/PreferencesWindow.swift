//
//  PreferencesWindow.swift
//  AutoHueControl
//
//  Created by Moe on 2018-06-11.
//  Copyright © 2018 Abushawish. All rights reserved.
//

import Cocoa
import SwiftyHue
import Gloss

class PreferencesWindow: NSWindowController {
    
    private let PREFERENCES_WINDOW_TITLE = "PreferencesWindow"
    
    @IBOutlet weak var lightsSelectionNSPB: NSMenu!
    @IBOutlet weak var deleteButtonNSB: NSButton!
    
    private var swiftyHue: SwiftyHue = SwiftyHue();
    private var selectedNSMenuItems = [LightNSMenuItem]()
    private var availableLights = [Light]()
    
    override var windowNibName: NSNib.Name? {
        return NSNib.Name(PREFERENCES_WINDOW_TITLE)
    }
    
    override func windowDidLoad() {
        super.windowDidLoad()
        
        self.window?.center()
        self.window?.makeKeyAndOrderFront(nil)
        NSApp.activate(ignoringOtherApps: true)
        
        if let accessConfig = HueController.readBridgeAccessConfig() {
            swiftyHue.setBridgeAccessConfig(accessConfig)
            let resourceAPI = swiftyHue.resourceAPI
            
            resourceAPI.fetchLights{(result) in
                if let lights = result.value {
                    for (_, light) in lights {
                        self.createLightMenuItem(light: light)
                        self.availableLights.append(light)
                    }
                    self.selectMenuItemsForPreviouslySelectedLights()
                }
            }
        }
    }
    
    override public func windowTitle(forDocumentDisplayName displayName: String) -> String {
        return PREFERENCES_WINDOW_TITLE
    }
    
    @IBAction private func savePreferencesButtonPress(_ sender: Any) {
        var selectedLights = [Light]()
        for lightNSMenuItem in selectedNSMenuItems {
            selectedLights.append(lightNSMenuItem.associatedLight)
        }
        HueController.writeLightsToSwitchOn(lightsToSwitchOn: selectedLights)
        self.close()
    }
    
    @IBAction private func deleteHueSettingsPressed(_ sender: Any) {
        dialogOKCancel(headerText: "Reset All Settings?", bodyText: "Would you like to reset all settings and re-setup AutoHueControl? This cannot be undone.")
    }
    
    private func dialogOKCancel(headerText: String, bodyText: String) {
        let alert = NSAlert()
        alert.messageText = headerText
        alert.informativeText = bodyText
        alert.alertStyle = .warning
        alert.icon = NSImage(named: NSImage.Name(rawValue: "Image"))
        alert.addButton(withTitle: "Reset AutoHueControl")
        alert.addButton(withTitle: "Cancel")
        alert.runAsPopover(for: deleteButtonNSB as NSView, withCompletionBlock: {
            (result : Int) in
            print(result)
            if (result == 1000) {
                UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                self.close()
                print("here")
            }
        })
    }
    
    /**
     Reads the UserDefaults for previously selected and saved lights.
     We then loop through the menu items and for each one check the
     menu item against all the saved ids list. We check if the menu item
     corresponds to the same light by comparing the hashvalues (selectLight.hashValue) and
     menuitem.tag. If these two match, we add the light to the selectedLights, selectedNSMenuItems,
     and then the menu item is visually selected.
     */
    private func selectMenuItemsForPreviouslySelectedLights() {
        if let previouslySelectedLightsIds = HueController.readLightsToSwitchOn() {
            for menuItem in lightsSelectionNSPB.items {
                if let lightMenuItem = menuItem as? LightNSMenuItem {
                    for selectedLightId in previouslySelectedLightsIds {
                        let selectLight = self.uniqueIdToLight(uniqueLightId: selectedLightId)
                        if (selectLight != nil && lightMenuItem.associatedLight.identifier == selectLight?.identifier) {
                            selectedNSMenuItems.append(lightMenuItem)
                            menuReselectMenuItems()
                        }
                    }
                }
            }
        }
    }
    
    private func createLightMenuItem(light: Light) {
        let lightMenuItem = LightNSMenuItem(title:light.name, action: #selector(lightSelected(_:)) , keyEquivalent:"", light: light)
        lightMenuItem.target = self
        lightsSelectionNSPB.addItem(lightMenuItem)
    }
    
    @objc private func lightSelected(_ sender: LightNSMenuItem) {
        if sender.tag != -1, let index = selectedNSMenuItems.index(of:sender) {
            selectedNSMenuItems.remove(at: index)
        } else {
            selectedNSMenuItems.append(sender)
        }
        
        menuReselectMenuItems()
    }
    
    public func bringToFront() {
        self.window?.center()
        self.window?.makeKeyAndOrderFront(nil)
        NSApp.activate(ignoringOtherApps: true)
    }
    
    private func menuReselectMenuItems() {
        menuUnselectAllMenuItems()
        menuSelectSelectedMenuItems()
    }
    private func menuUnselectAllMenuItems() {
        for allMenuItems in lightsSelectionNSPB.items {
            allMenuItems.state = NSControl.StateValue.off
        }
    }
    
    private func menuSelectSelectedMenuItems() {
        for selectedItem in selectedNSMenuItems {
            print(selectedItem.title)
            selectedItem.state = NSControl.StateValue.on
        }
    }
    
    private func uniqueIdToLight(uniqueLightId: String) -> Light? {
        var lightToReturn: Light?
        for light in availableLights {
            if (light.identifier == uniqueLightId) {
                lightToReturn = light
            }
        }
        return lightToReturn
    }
}
