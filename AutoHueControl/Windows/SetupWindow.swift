//
//  SetupPreferencesWindow.swift
//  AutoHueControl
//
//  Created by Moe on 2018-06-08.
//  Copyright © 2018 Abushawish. All rights reserved.
//

import Cocoa
import SwiftyHue
import Gloss
import StoreKit
private var swiftyHue: SwiftyHue = SwiftyHue();
class SetupWindow: NSWindowController, BridgeFinderDelegate, BridgeAuthenticatorDelegate {
    
    private let SETUP_WINDOW_TITLE = "SetupPreferencesWindow"
    
    private var timer: Timer?
    private var bridgeFinder: BridgeFinder?
    private var bridgeAuthenticator: BridgeAuthenticator?
    private var hugeBridgeConfig: HueBridgeConfigModel?
    private var hueBridgeCountdownStart = 35.0
    private var completionHandler: ((Bool)->())?
    
    @IBOutlet weak var failedCheckMarkNSTF: NSTextField!
    @IBOutlet weak var completedCheckMarkNSTF: NSTextField!
    @IBOutlet weak var hueBridgeProgressNSPI: NSProgressIndicator!
    @IBOutlet weak var cancelBridgeSetupTB: TextButton!
    @IBOutlet weak var retryBridgeSetupTB: TextButton!
    @IBOutlet weak var nextBridgeSetupTB: TextButton!
    
    init(completionHandler:@escaping (Bool) -> ()) {
        super.init(window: nil)
        self.completionHandler = completionHandler
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override var windowNibName: NSNib.Name? {
        return NSNib.Name(SETUP_WINDOW_TITLE)
    }

    override func windowDidLoad() {
        super.windowDidLoad()
        hugeBridgeConfig = HueBridgeConfigModel()
        bridgeFinder = BridgeFinder()
        timer = Timer()
        
        bridgeConnectStartViews()

        if let bridgeAccessConfig = HueController.readBridgeAccessConfig() {
            swiftyHue.setBridgeAccessConfig(bridgeAccessConfig)
            completionHandler?(true)
        } else {
            startCountdownProgress(beginningValue: hueBridgeCountdownStart)
            bridgeFinder?.delegate = self;
            bridgeFinder?.start()
        }
        
        self.window?.center()
        self.window?.makeKeyAndOrderFront(nil)
        NSApp.activate(ignoringOtherApps: true)
    }
    
    override func windowTitle(forDocumentDisplayName displayName: String) -> String {
        return SETUP_WINDOW_TITLE
    }
    
    @IBAction func cancelBridgeSetupClick(_ sender: Any) {
        invalidateTimer()
        self.close()
    }
    
    @IBAction func nextBridgeSetupClick(_ sender: Any) {
        completionHandler?(true)
    }
    
    @IBAction func retryBridgeSetupClick(_ sender: Any) {
        startCountdownProgress(beginningValue: hueBridgeCountdownStart)
        bridgeConnectStartViews()
        bridgeFinder?.start()
    }
    
    func bridgeFinder(_ finder: BridgeFinder, didFinishWithResult bridges: [HueBridge]) {
        guard let bridge = bridges.first else {
            bridgeConnectFailedViews()
            return
        }
        
        hugeBridgeConfig?.uuid = bridge.UDN
        hugeBridgeConfig?.ip = bridge.ip
        
        bridgeAuthenticator = BridgeAuthenticator(bridge: bridge, uniqueIdentifier: HueController.BRIDGE_AUTHENTIC_APP_ID)
        
        bridgeAuthenticator!.delegate = self
        bridgeAuthenticator!.start()
    }
    
    func bridgeAuthenticator(_ authenticator: BridgeAuthenticator, didFinishAuthentication username: String) {
        hugeBridgeConfig?.username = username
        
        let bridgeConfig = BridgeAccessConfig(bridgeId: (hugeBridgeConfig?.uuid)!, ipAddress: (hugeBridgeConfig?.ip)!, username: (hugeBridgeConfig?.username)!)
        HueController.writeBridgeAccessConfig(bridgeAccessConfig: bridgeConfig)
        self.invalidateTimer()
        bridgeConnectSuccessViews()
    }
    
    func bridgeAuthenticator(_ authenticator: BridgeAuthenticator, didFailWithError error: NSError) {
        self.invalidateTimer()
        self.bridgeConnectFailedViews()
    }
    
    func bridgeAuthenticatorRequiresLinkButtonPress(_ authenticator: BridgeAuthenticator, secondsLeft: TimeInterval) {
    }
    
    func bridgeAuthenticatorDidTimeout(_ authenticator: BridgeAuthenticator) {
        self.invalidateTimer()
        self.bridgeConnectFailedViews()
    }
    
    private func startCountdownProgress(beginningValue: Double) {
        let fireInterval: TimeInterval = 0.01
        var elapsedTime = hueBridgeCountdownStart
        
        timer = Timer(timeInterval: fireInterval, repeats: true) { [unowned self] _ in
            elapsedTime += (fireInterval * -1)

            if elapsedTime > 0 {
                self.hueBridgeProgressNSPI.doubleValue = elapsedTime
            } else {
                self.invalidateTimer()
                self.bridgeConnectFailedViews()
            }
        }
        RunLoop.main.add(timer!, forMode: .defaultRunLoopMode)
    }

    private func invalidateTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    private func bridgeConnectSuccessViews() {
        cancelBridgeSetupTB.isHidden = true
        hueBridgeProgressNSPI.isHidden = true
        failedCheckMarkNSTF.isHidden = true
        retryBridgeSetupTB.isHidden = true
        nextBridgeSetupTB.isHidden = false
        completedCheckMarkNSTF.isHidden = false
    }
    
    private func bridgeConnectFailedViews() {
        completedCheckMarkNSTF.isHidden = true
        cancelBridgeSetupTB.isHidden = true
        nextBridgeSetupTB.isHidden = true
        hueBridgeProgressNSPI.isHidden = true
        failedCheckMarkNSTF.isHidden = false
        retryBridgeSetupTB.isHidden = false
    }
    
    private func bridgeConnectStartViews() {
        completedCheckMarkNSTF.isHidden = true
        failedCheckMarkNSTF.isHidden = true
        nextBridgeSetupTB.isHidden = true
        retryBridgeSetupTB.isHidden = true
        hueBridgeProgressNSPI.isHidden = false
        cancelBridgeSetupTB.isHidden = false
        hueBridgeProgressNSPI.doubleValue = hueBridgeCountdownStart
    }
    
    public func bringToFront() {
        self.window?.center()
        self.window?.makeKeyAndOrderFront(nil)
        NSApp.activate(ignoringOtherApps: true)
    }
}
