//
//  HueController.swift
//  AutoHueControl
//
//  Created by Moe on 2018-06-08.
//  Copyright © 2018 Abushawish. All rights reserved.
//

import Foundation
import SwiftyHue
import Gloss

class HueController {
    
    private static let bridgeAccessConfigUserDefaultsKey = "BridgeAccessConfigAutoHue7"
    private static let lightIdsToTurnOnUserDefaultsKey = "LightIdsToTurnOnAutoHue7"
    public static let BRIDGE_AUTHENTIC_APP_ID = "autoHueControl7"
    
    /**
     Stores the BridgeAccessConfig that contains the bridge address
     */
    static func writeBridgeAccessConfig(bridgeAccessConfig: BridgeAccessConfig) {
        let userDefaults = UserDefaults.standard
        let bridgeAccessConfigJSON = bridgeAccessConfig.toJSON()
        userDefaults.set(bridgeAccessConfigJSON, forKey: bridgeAccessConfigUserDefaultsKey)
    }
    
    /**
     Reads the BridgeAccessConfig that contains the bridge address
     */
    public static func readBridgeAccessConfig() -> BridgeAccessConfig? {
        let userDefaults = UserDefaults.standard
        let bridgeAccessConfigJSON = userDefaults.object(forKey: bridgeAccessConfigUserDefaultsKey) as? JSON
        
        var bridgeAccessConfig: BridgeAccessConfig?
        if let bridgeAccessConfigJSON = bridgeAccessConfigJSON {
            bridgeAccessConfig = BridgeAccessConfig(json: bridgeAccessConfigJSON)
        }
        
        return bridgeAccessConfig
    }
    
    /**
     Stores lights array that should turn on
     */
    public static func writeLightsToSwitchOn(lightsToSwitchOn: [Light]) {
        var lightIdsToSwitchOn = [String]()
        for light in lightsToSwitchOn {
            lightIdsToSwitchOn.append(light.identifier)
        }
        let userDefaults = UserDefaults.standard
        userDefaults.set(lightIdsToSwitchOn, forKey: lightIdsToTurnOnUserDefaultsKey)
    }
    
    /**
     Reads light ids array that should turn on
     */
    public static func readLightsToSwitchOn() -> [String]? {
        let userDefaults = UserDefaults.standard
        let lightIdsToSwitchOn = userDefaults.array(forKey: lightIdsToTurnOnUserDefaultsKey) as? [String] ?? [String]()
        
        return lightIdsToSwitchOn
    }
    
    public static func toggleSavedLights(onOff: Bool) {
        if (readBridgeAccessConfig() == nil) {
            print("readBridgeAccessConfig() returns nil, can not toggle lights.")
            return
        }
        
        if let lightsArray = readLightsToSwitchOn() {
            for stringLight in lightsArray {
                HueController.toggleGivenLight(lightId: stringLight, onOff: onOff)
            }
        }
    }
    
    private static func toggleGivenLight(lightId: String, onOff: Bool) {
        let swiftyHue = SwiftyHue();
        var lightState = LightState()
        lightState.on = onOff
        let bridgeAccessConfig = HueController.readBridgeAccessConfig()!
        swiftyHue.setBridgeAccessConfig(bridgeAccessConfig)
        swiftyHue.bridgeSendAPI.updateLightStateForId(lightId, withLightState:lightState) { (errors) in
            if (errors != nil) {
                print(errors!)
            }
        }
    }
}
